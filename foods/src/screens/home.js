import React from "react"
import {View, Text, TouchableOpacity} from "react-native"
import Form from "../components/Form"
import FontAwesome from "react-native-vector-icons/FontAwesome"

function HomeScreen() {
  const onSubmit = data => console.log(data)
  const listInput = [
    {
      key: 1,
      group: true,
      name: "user",
      styleGroup: {
        flexDirection: "row",
        justifyContent: "space-between",
      },
      commonStyleInputGroup: {
        color: "red",
        paddingStart: 40,
      },
      listInputGroup: [
        {
          key: 2,
          name: "name1",
          styleBoxInput: {
            width: "47.5%",
          },
          icon: <FontAwesome name="user" color={"royalblue"} size={20} />,
        },
        {
          key: 3,
          name: "name2",
          styleBoxInput: {
            width: "47.5%",
          },
          icon: <FontAwesome name="user" color={"royalblue"} size={20} />,
        },
      ],
    },
    {
      key: 4,
      name: "password",
      styleInput: {
        paddingStart: 40,
      },
      icon: <FontAwesome name="user" color={"royalblue"} size={20} />,
    },
  ]
  return (
    <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
      {/* <Form listInput={listInput} onSubmit={onSubmit} /> */}
    </View>
  )
}
export default HomeScreen
