import * as React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Router from './router';
import {Provider} from 'react-redux';
import configureStore from './src/redux/configureStore';

const store = configureStore();

const Stack = createStackNavigator();

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          {Router.map(router => {
            return (
              <Stack.Screen
                key={router.key}
                name={router.name}
                component={router.component}
              />
            );
          })}
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
