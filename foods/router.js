import BottomTabNavigation from "./src/bottomTabNavigation/bottomTabMain"

const ROUTER = [
    {
        key: 1,
        name: "Food",
        component: BottomTabNavigation
    }
]

export default ROUTER